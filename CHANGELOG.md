## [2.4] - 2019-06-04

### Changed

- Update to Forge 0.8.4.
- Replace Forge with a non-minified version. (#16686)
- Only include the SHA-256 module of Forge.

### Documentation

- Document how to build Forge instead of download it.

## [2.3] - 2019-02-13

### Changed

- Prevent fingerprint using Web Accessible Resources. (#14787)

## [2.2] - 2019-02-13

### Changed

- Fix running the extension after installing on Chrome.

## [2.1] - 2019-02-13

### Changed

- Fix refreshing the page when the extension is installed on Chrome by
  sending 'extension-installed' instead of reloading the page. (#16078)
- Refresh the page on Chrome when testing locally as well.
- Update to Forge 0.8.0.

## [2.0] - 2018-12-25

### Changed

- Code rewrite:
  - Use modern JS promises (await/async functions).
  - Get rid of global objects.
  - Improve error logging.
  - Comment the code better.
  - Satisfy JSLint.
- Move background scripts to content scripts.
- Use & fetch JSON file instead of YAML as IDF:
  - Replace support for IDF v1 by support for IDF v2.
  - Check if users validate ISO or IMG files.
  - Review the entire JSON SHA checksum verification logic.
- Get rid of jQuery.
- Implement a better way to test the extension locally:
  - Delete local patch on the extension.
  - Adjust README to modify permissions in manifest.json.
  - Move local testing logic to variable has_local_permissions.

### Security

- Restrict loading of the extension's scripts to https://tails.boum.org/install/*
  and https://tails.boum.org/upgrade/*.

## [1.2] - 2018-11-13

### Changed

- Remove patches for testing locally that were included by mistake in 1.1.

## [1.1] - 2018-11-13

### Changed

- Relax CSP to fix refreshing download page on installation. (#16078)
- Update to Forge 0.7.6.

## [1.0] - 2018-03-21

### Changed

- Update to Forge 0.7.4.
- Update to jQuery 3.3.1.
- Improve description and icons for app stores.
- Improve error message when failing to fetch IDF.
- Remove useless debugging code.

### Security

- Use a restrictive content security policy: "default-src 'none'".
- Remove message listening code.

### Documentation

- Complete licensing information.
- Document how to test locally on Chrome.
- Document how to update third-party libraries.
- Write a manual test suite.
- Improve release process to Firefox and Chrome app stores from Tails.

## [0.95] - 2017-12-03

### Changed

- Remove output to console outside of errors.

### Security

- Check that `https://tails.boum.org/` is the prefix of the tab's URL
  before injecting background.js.

### Documentation

- Automate changing the version number in manifest.json.
- Add licensing info of Forge library.
- Add copyright notice.

## [0.94] - 2017-11-22

### Added

- Add patches to test the extension locally.

### Security

- Set `https://tails.boum.org/` as target origin for `postMessage`.

### Documentation

- Document the release process.

## [0.93] - 2017-11-17

### Changed

- Switch to checking the `amd64` IDF instead of the `i386` IDF.
- Comply with JSLint.

### Documentation

- Add README.
- Document source URL and version of Forge library.

## [0.92] - 2017-11-14

### Added

- Write initial version.
