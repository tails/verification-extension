/*
 * Only needed in Chrome for reloading the page right after installing the plugin.
 * It will also reload open tabs if the plugin is updated.
 * This is not needed in Firefox.
 * */
chrome.runtime.onInstalled.addListener(function() {
    if (typeof browser == "undefined") { // this is Chrome
        let has_local_permissions = chrome.runtime.getManifest().permissions.includes("file:///"); // for local testing
        let urls = ["https://tails.boum.org/install/*", "https://tails.boum.org/update/*"]
        if (has_local_permissions) {
            urls.push("file:///*");
        }
        chrome.tabs.query({ url: urls }, function(tabs) {
            for (let tab of tabs) {
                chrome.tabs.executeScript(tab.id, {code: "window.location.hash = '#chrome-installation'; window.location.reload();"});
            }
        });
    }
});
