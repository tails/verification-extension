/*
 * Copyright 2015-2018, Tails developers and Giorgio Maone <tails@boum.org>, Enrico Zini <enrico@enricozini.org>
 * */

(function() {
"use strict";

/*
 * Parse and access the release information published by Tails
 * */
class Expected {
    constructor(conf){
        this.conf = conf;
        this.expected = null; // Data we use to validate the file downloaded by the user
    }

    /*
     * Ajax promise which reads scripts/contentscript/conf.js, fetches IDF from
     * URL in JSON format, and processes its contents.
     * @loads: config data as `expected`
     * */
    async load() {
        let data = await fetch(this.conf.descriptor)
                         .then(function(response) {
                             if (response.ok) {
                                 return response.json();
                             } else {
                                 throw new Error("Error fetching file.");
                             }
                         });
        this.expected = data;
        console.log("JSON data:", data);
        console.log("Conf fetched from ", this.conf.descriptor);
    }

    /*
     * Returns found hashes from IDF.
     * Comparing file to be validated with what's listed in the IDF:
     * if size and type of a file correspond, we return a list of possible hashes.
     * expects File object.
     * */
    lookup(file) {

        // TODO: Add version lookup.
        // If we add older versions to the IDF, we might want to add a version check here.
        // This could happen either by matching the file name
        // or by matching the version number and alerting the user that they are trying to validate an older version of Tails.
        // This could also be achieved by adding a "deprecated" flag into the JSON.

        let hashes = [];

        console.log("File size of downloaded file: ", file.size);
        console.log("File type of downloaded file: ", file.type);

        let type = null;
        if (file.type == "application/x-cd-image" || (file.name).match(/\.iso$/i)) {
            type = "iso";
        } else if (file.type == "application/x-raw-disk-image" || (file.name).match(/\.img$/i)) {
            type = "img";
        } else {
            return hashes;
        }

        for (const installation of this.expected.installations) {
            for (const path of installation["installation-paths"]) {
                if (path.type != type) {
                    continue;
                }
                for (const target of path["target-files"]) {
                    if (target.size != file.size) {
                        continue;
                    }
                    hashes.push(target.sha256); // size and type correspond to a file listed in the IDF
                }
            }
        }
        return hashes;
    }
}

class VerifyDownload {
    constructor(conf){
        this.conf = conf;
        this.expected = new Expected(conf); // Data we use to validate the file downloaded by the user
        this.extVersion = chrome.runtime.getManifest().version;
        this.has_local_permissions = chrome.runtime.getManifest().permissions.includes("file:///"); // for local testing
        this.lastCalculatedPercentage = 0;
        // Start init. This will be awaited by validateFile, when the results will actually be needed
        this.initializing = this.init();

        document.getElementById(this.conf.verifySelector).addEventListener("change", (e) => {
            this.validateFile(e.target).then();
        }, false);
    }

    async init() {
        this.postMessage({ action: "extension-installed" });

        // check if browser supports native FileAPI and managed to download &
        // process the IDF.
        if (!this.checkFileAPI()) {
            // It is likely that a browser that can run this JavaScript also
            // has the file API. It may make sense to move browser checks to
            // code executed earlier, written in an older version of
            // JavaScript. It could also be that all browsers where this is
            // supposed to run are already modern enough, and no check is
            // needed at all.
            console.error("FileReader API not supported");
            return;
        }

        try {
            await this.expected.load();
        } catch (e) {
            console.error("Failed to download IDF", e);
            return;
        }
    }

    /*
     * Check if native web API for files are supported in the browser.
     * The File interface provides information about files and allows JavaScript in a web page to access their content.
     * FileReader object lets web apps asynchronously read contents of files stored on the user's computer.
     * https://developer.mozilla.org/en-US/docs/Web/API/FileReader
     * @returns true|false.
     * */
    checkFileAPI() {
        return (window.File && window.FileReader && window.FileList && window.Blob);
    }

    /*
     * reads a portion of a file, returning a promise resolving to the file data read
     */
    readChunk(file, chunk_offset, chunk_size) {
        return new Promise(function(resolve, reject) {
            let fr = new FileReader();
            fr.onload = e => {
                resolve(e.target.result);
            };

            // on error, reject the promise
            fr.onerror = (e) => {
                reject(e);
            };
            let slice = file.slice(chunk_offset, chunk_offset + chunk_size);

            // This API is non-standard: https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsBinaryString
            // We use it for performance reasons, see #15059.
            fr.readAsBinaryString(slice);
        });
    }

    /*
     * Progressively read a file that was downloaded to the user's computer,
     * compute its sha256sum, and report on progress.
     *
     * Takes a JavaScript File object as input.
     * https://developer.mozilla.org/en-US/docs/Web/API/File
     */
    async readFile(file) {
        const CHUNK_SIZE = 2 * 1024 *1024;
        let offset = 0;
        while (true)
        {
            const chunk = await this.readChunk(file, offset, CHUNK_SIZE);
            // TODO: maybe do not compute sha synchronously, but in a worker
            // thread see branch `async_shasum`
            this.sha256.update(chunk);
            offset += chunk.length;
            if (chunk.length < CHUNK_SIZE)
                return;

            const progressPercent = parseInt(offset * 100.0 / file.size);
            if (progressPercent != this.lastCalculatedPercentage) {
                this.postMessage({ action: "progress", percentage: progressPercent});
                this.lastCalculatedPercentage = progressPercent;
            }
        }
    }

    /*
     * Verify file size and hashsum of downloaded file.
     * We do not process the outcome of this function any further than displaying it to the user.
     * @returns: void.
     * */
    async validateFile(filePath) {
        // Make sure the init() that was started in the constructor actually
        // finishes
        await this.initializing;

        if (!filePath.files || !filePath.files[0])
            return;
        let file = filePath.files[0];

        console.log("File to be verified: ", file.name);
        this.postMessage({ action: "verifying", fileName:file.name});

        let hashes = this.expected.lookup(file);
        console.log("Candidate hashes found in IDF:", hashes);
        if (hashes.length == 0) {
            console.error("File size or type does not match any file in the IDF.");
            this.postMessage({ action: "verification-failed" });
            return; // stop processing here.
        }

        // This is the most time consuming task: for benchmarking during
        // development, measure how long it takes
        const time_start = new Date();
        // read the file and compute its sha256
        this.sha256 = forge.md.sha256.create();
        try {
            await this.readFile(file);
        } catch (e) {
            console.log("Error reading file", e);
            // FIXME: it would be nice to tell the user if the verification
            // failed because of a shasum mismatch or because of a I/O error.
            // Maybe there can be extra arguments to this postMessage?
            this.postMessage({ action: "verification-failed" });
            return;
        }
        const time_end = new Date();
        console.log("Elapsed:", time_end - time_start, "ms");

        // compare sha256 created using forge for downloaded file with expected sha256 from IDF
        let computed_hash = this.sha256.digest().toHex();
        console.log("Computed hash of downloaded file: ", computed_hash);

        // look up expected (from IDF) to match a hash and if true mark as found
        let found = false;
        for (const h of hashes) {
            if (h == computed_hash) {
                console.log("Found hash in IDF", h);
                found = true;
                break;
            }
        }

        if (found) {
            this.postMessage({ action: "verification-success" });
        } else {
            console.error("File hash does not match");
            this.postMessage({ action: "verification-failed" });
        }
    }

    /* Generic hack to make sure that the message is only received by pages of
     * 'https://tails.boum.org', and not disclosed to any page or party which
     * does not have the same protocol, port and origin.
     * */
    postMessage(data, has_local_permissions) {
        if (this.has_local_permissions) {
            console.log("Using local permissions.");
            window.postMessage(data, "*");
        } else {
            window.postMessage(data, "https://tails.boum.org");
        }
    }
}

/*
 * This allows the tails website to specify, via a div, a minimum plugin
 * version needed to verify the files.
 * */
function checkExtensionVersion() {
    // TODO: move this to library code, and unit test it
    function compareVersions(v1, v2) {
        if (v1 === v2) {
            return 0;
        }
        let [a, b] = [v1, v2].map(s => String(s).split("."));
        for (let j = 0, len = Math.max(a.length, b.length); j < len; j++) {
            let [x, y] = [a[j], b[j]].map(s => s || "0");
            let [n1, n2] = [x, y].map(s => parseInt(s, 10));
            if (n1 > n2) {
                console.log("Newer version installed.");
                return 1;
            }
            if (n1 < n2) {
                console.log("Older version installed.");
                return -1;
            }
            [x, y] = [x, y].map(s => s.match(/[a-z].*/i));
            if (x || y) {
                if (x && y) return x[0] < y[0] ? -1 : x[0] > y[0] ? 1 : 0;
                return x ? -1 : 1;
            }
        }
        return 0;
    }

    // stop processing here if we cannot read the existing extension version
    // this would also fail if the div does not exist.
    let extension_version = document.getElementById('extension-version').innerHTML;
    if (!extension_version) {
        console.error("Tails Verification version information unavailable.");
        return false;
    }

    let manifest = chrome.runtime.getManifest();
    let versionCmp = compareVersions(manifest.version, extension_version);
    let ok = versionCmp >= 0; // if we have the same or a newer version of the extension installed
    document.documentElement.dataset.extension = ok ? "up-to-date" : "outdated";
    return ok;
}

/*
 * On the download page, check for the existence of #activate-tails-verification.
 * If extension version is up-to-date, proceed to verify the downloaded files.
 * */
if (document.getElementById('activate-tails-verification') !== null) {
    if (!checkExtensionVersion()) {
        document.documentElement.removeAttribute("data-phase");
    }
    let verify = new VerifyDownload(conf);
} else {
    console.log("Verification extension cannot proceed, div#activate-tails-verification is missing.");
}

})();
